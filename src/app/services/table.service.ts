import { Injectable, PipeTransform } from '@angular/core';
import { Country, COUNTRIES } from '../interfaces/interfaces';
import { DecimalPipe } from '@angular/common';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { switchMap } from 'rxjs/operators';

interface SearchResult {
  countries: Country[];
  total: number;
}

interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
}

function matches(country: Country, term: string, pipe: PipeTransform) {
  return (
    country.name.toLowerCase().includes(term.toLowerCase()) ||
    pipe.transform(country.area).includes(term) ||
    pipe.transform(country.population).includes(term)
  );
}

@Injectable({
  providedIn: 'root',
})
export class TableService {
  private searchVar$ = new Subject<void>();
  private countriesVar$ = new BehaviorSubject<Country[]>([]);
  private totalVar$ = new BehaviorSubject<number>(0);

  private stateVar: State = {
    page: 1,
    pageSize: 4,
    searchTerm: '',
  };

  constructor(private pipe: DecimalPipe) {
    this.searchVar$.pipe(switchMap(() => this.searchVar())).subscribe(result => {
      this.countriesVar$.next(result.countries);
      this.totalVar$.next(result.total);
    });

    this.searchVar$.next();
  }

  get countries$() {
    return this.countriesVar$.asObservable();
  }
  get total$() {
    return this.totalVar$.asObservable();
  }
  get page() {
    return this.stateVar.page;
  }
  set page(page: number) {
    this.setVar({ page });
  }
  get pageSize() {
    return this.stateVar.pageSize;
  }
  set pageSize(pageSize: number) {
    this.setVar({ pageSize });
  }
  get searchTerm() {
    return this.stateVar.searchTerm;
  }
  set searchTerm(searchTerm: string) {
    this.setVar({ searchTerm });
  }

  private setVar(patch: Partial<State>) {
    Object.assign(this.stateVar, patch);
    this.searchVar$.next();
  }

  private searchVar(): Observable<SearchResult> {
    const { pageSize, page, searchTerm } = this.stateVar;

    // 1. filter
    let countries = COUNTRIES.filter(country => matches(country, searchTerm, this.pipe));
    const total = countries.length;

    // 2. paginate
    countries = countries.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
    return of({ countries, total });
  }
}
