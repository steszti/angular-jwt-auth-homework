import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent {
  status = this.auth
    .getStatus()
    .pipe(take(1))
    .subscribe(
      res => (this.status = res.status),
      err => (this.status = err.statusText),
    );

  constructor(private auth: AuthService) {}
}
