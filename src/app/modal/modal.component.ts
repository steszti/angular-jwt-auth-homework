import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent {
  images = [600, 470, 530, 520, 560].map(n => `http://placekitten.com/${n}/300`);
  constructor(public activeModal: NgbActiveModal) {}
}
