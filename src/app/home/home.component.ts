import { Component, OnInit } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { Observable } from 'rxjs';
import { Country } from '../interfaces/interfaces';
import { TableService } from '../services/table.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [TableService, DecimalPipe],
})
export class HomeComponent implements OnInit {
  countries$: Observable<Country[]>;
  total$: Observable<number>;

  constructor(public service: TableService, private modalService: NgbModal) {
    this.countries$ = service.countries$;
    this.total$ = service.total$;
  }

  open() {
    this.modalService.open(ModalComponent);
  }

  ngOnInit() {}
}
