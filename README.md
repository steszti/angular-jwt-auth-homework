## Goal
The main goal of the app is to create an **Angular** application, which is able to authenticate a user with a mock backend provider (provided by Ákos).
[https://gitlab.com/hegelakos/simple-auth](https://gitlab.com/hegelakos/simple-auth) 

## Basics
The app should have a **Login**, **Home**, and a **Profile** page, and a navigation bar at the top. The user can freely navigate between the Login and Home pages, but won't be able to reach the Profile page unless authenticated. For this purpose use a **route guard**.

## Login
The login page should contain a **form** ([Angular forms](https://angular.io/start/start-forms)) with two controls: **username** (or email) and **password**. Use basic validation, meaning that both fields are required, and the password should be at least 8 characters long.
Occurring errors must be displayed to the user.

## Home
The Home page should be the default page, which contains some placeholder text, organized into 3 **accordions** next to each other on the top, and a full-width **table** under them with *filtering* and *pagination*, which is filled with some mock data.
There should be a button that opens up a **modal**, which displays a **carousel** (image gallery) with some placeholder images. 
Use [placekitten](https://placekitten.com/) for cute and easy to use images.

Use components provided by [ng-bootstrap](https://ng-bootstrap.github.io/#/home) and bootstrap helper classes for site-building (.scss files should only be modified if really necessary).

## Profile
This page is only accessible once the user has logged in. Otherwise it is not even shown on the navbar.
The data coming from the authenticated status endpoint should be displayed here on a simple bootstrap card component.

## Auth Service
Generate an authentication service with the **Angular CLI**. 
This service should implement 3 methods: **login**, **logout**, and **isLoggedIn**. (additional helper methods can be implemented as well)
The token returned from the backend should be decoded by using the npm package: [angular-jwt](https://www.npmjs.com/package/@auth0/angular-jwt) and stored on the frontend respectively (in localStorage).
If the token expires, the user should be logged out immediately.

## Links and Useful Stuff

Simple Auth backend: [https://gitlab.com/hegelakos/simple-auth](https://gitlab.com/hegelakos/simple-auth)  - follow instructions in readme to set up.

The project we created during the lessons can be used as a reference point: [https://gitlab.com/baaalint/cc-2020/-/tree/develop](https://gitlab.com/baaalint/cc-2020/-/tree/develop)

[Bootstrap  - Getting Started](https://ng-bootstrap.github.io/#/getting-started)

[Angular Auth - (JWT) Interceptor](https://medium.com/@ryanchenkie_40935/angular-authentication-using-the-http-client-and-http-interceptors-2f9d1540eb8)

[Angular JWT Auth](https://www.techiediaries.com/angular/jwt-authentication-angular-9-example/)

[Angular Auth - Route Guards](https://medium.com/@ryanchenkie_40935/angular-authentication-using-route-guards-bf7a4ca13ae3)


[JWT - Official Docs](https://jwt.io/)

[https://www.npmjs.com/package/@auth0/angular-jwt](https://www.npmjs.com/package/@auth0/angular-jwt)